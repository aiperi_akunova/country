import './App.css';
import {useEffect, useState} from "react";
import axios from "axios";

const url = 'https://restcountries.eu/rest/v2/all';
const codeUrl = 'https://restcountries.eu/rest/v2/alpha/';

const App = () => {
    const [countries, setCountries] = useState([]);
    const [borders, setBorders] = useState([]);
    const [borderCountries, setBorderCountries] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(url);
            const allCountries = response.data;
            const countryNames = [];
            allCountries.forEach(country => {
                countryNames.push({name: country.name, alphaCode: country.alpha3Code, capital: country.capital});
            });
            setCountries(countryNames);
        }
        fetchData().catch(e => console.error(e));

    }, []);

    useEffect(() => {
        const fetchData = async () => {

            const promises = borders.map(countryCode => {
                return axios.get(codeUrl +
                    countryCode);
            })
            const results = await Promise.all(promises);

            const borderCountries = results.map(result => {
                return result.data.name;
            })
            setBorderCountries(borderCountries);
        }
        fetchData().catch(e => console.error(e));

    }, [borders]);

    const requestOnClick = (code) => {
        const fetchData = async () => {
            const response = await axios.get('https://restcountries.eu/rest/v2/alpha/' + code);
            const borders = response.data.borders;
            setBorders(borders);
        }
        fetchData().catch(e => console.error(e));
    }

    return (
        <div className="App">
            <div className='boxes'>
                <div className='box-1'>

                    <ul className='list-countries'>
                        {countries.map(c => (
                            <li className='list'
                                id={c.alphaCode}
                                key={c.name}
                                onClick={(e) => requestOnClick(e.target.id)}>
                                {c.name}
                            </li>
                        ))}
                    </ul>
                </div>
                <div className='box-2'>
                    <h2>Information about the country</h2>
                    <p><b>Borders:</b></p>
                    <ul>
                        {borderCountries.map((b, i) => (
                            <li key={i}>{b}</li>
                        ))}
                    </ul>
                </div>
            </div>

        </div>
    );
};

export default App;
